const path = require('path');
const srcPath = path.resolve(__dirname, "../src");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: false,
        modules: true,
        localIdentName: '[hash:base64:5]',
    },
};

module.exports = {
    mode: "production",
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "../builds/"),
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                include: [srcPath],
                loader: "babel-loader",
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                include: [srcPath],
                enforce: "pre",
                use: [
                    "style-loader",
                    cssLoader,
                    "postcss-loader"
                ],
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                enforce: "post",
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                include: [srcPath],
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 40960,
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '*'],
        alias: {
            src: srcPath,
        }
    },
    plugins: [
        new CleanWebpackPlugin(['/builds'], {allowExternal: true, beforeEmit: true,}),
        new MiniCssExtractPlugin({
            filename: 'style.css',
        }),
        new HtmlWebpackPlugin({
            title: 'My App',
            filename: 'index.html',
            template: 'public/index.html'
        }),
    ]
};