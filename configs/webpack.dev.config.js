const path = require('path');
const srcPath = path.resolve(__dirname, "../src");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: true,
        modules: true,
        localIdentName: '[name]__[local]__[hash:base64:5]',
    },
};

module.exports = {
    mode: "development",
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "../builds/"),
        filename: "bundle.js",
        publicPath: '/',
    },
    module: {
        rules: [
            // {
            //     include: [srcPath],
            //     test: /\.(js|jsx)$/,
            //     enforce: 'pre',
            //     exclude: /node_modules/,
            //     loader: "eslint-loader"
            // },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                include: [srcPath],
                loader: "babel-loader",
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                include: [srcPath],
                enforce: "pre",
                use: [
                    "style-loader",
                    cssLoader,
                ],
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                enforce: "post",
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                include: [srcPath],
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 40960,
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '*'],
        alias: {
            src: srcPath,
        }
    },
    devServer: {
        contentBase: path.join(__dirname, 'builds'),
        inline: true,
        hot: true,
        https: false,
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'My App',
            filename: 'index.html',
            template: 'public/index.html'
        }),
        new webpack.HotModuleReplacementPlugin(),
    ]
};