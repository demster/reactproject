import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import getBeejeeInitialState from '../../Beejee/store/BeejeeInitialState';
import rootReducers from '../reducers/rootReducers';
import composeWithDevTools from 'redux-devtools-extension';

export default function getReduxStore() {
	const initialState = {
		beejeeState: getBeejeeInitialState(),
	};
	const middlewares = [thunk];

	return createStore(rootReducers, initialState, applyMiddleware(...middlewares));
}