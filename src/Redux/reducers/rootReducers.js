import {combineReducers} from 'redux';
import beejeeReducer from '../../Beejee/reducers/beejeeReducer';

export default combineReducers({
	beejeeState: beejeeReducer,
});