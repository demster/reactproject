import React from 'react'
import ReactDOM from 'react-dom'
import style from './index.css'
import ProfitCenterFx from "./ProfitCenterFx/ProfitCenterFx"
import 'bootstrap/dist/css/bootstrap.min.css'

const div = document.createElement('div');
div.setAttribute('id', 'main');
div.setAttribute('class', style['main']);
document.body.appendChild(div);

ReactDOM.render(
    <ProfitCenterFx/>,
    div
);