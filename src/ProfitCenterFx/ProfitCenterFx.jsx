import React from 'react'
import {TabContent} from 'reactstrap'
import style from './ProfitCenterFx.css'
import ProfitCenterLoader from "./components/ProfitCenterLoader/ProfitCenterLoader"
import update from 'immutability-helper'
import ProfitCenterTabs from "./components/ProfitCenterTabs/ProfitCenterTabs"
import {
    getAvg, getListByTaskOne,
    getMedianValue,
    getModeValue,
    getRandomInt,
    getStandardDeviation
} from "./helpers/helper"
import ProfitCenterTabOne from "./components/ProfitCenterTabOne/ProfitCenterTabOne"
import ProfitCenterTabTwo from "./components/ProfitCenterTabTwo/ProfitCenterTabTwo"
import Ping from 'ping.js';

class ProfitCenterFx extends React.Component {
    state = {
        minValue: '',
        maxValue: '',
        mathInfo: {},
        activeTab: '0',
        warnings: {
            minMoreMax: {
                value: false,
                text: 'Минимальное значение должно быть меньше максимального',
                color: 'warning'
            },
            emptyMin: {
                value: false,
                text: 'Заполните минимальное значение',
                color: 'warning'
            },
            emptyMax: {
                value: false,
                text: 'Заполните максимальное значение',
                color: 'warning'
            },
        },
        errors: {
            memory: {
                value: false,
                text: 'Не удалось сгенерировать случайные числа. Пожалуйста повторите попытку или уменьшите диапазон',
                color: 'danger',
            },
        },
        listOne: [],
        host: '',
        listTwo: [],
        maskList: [
            {value: 'http://', key: '0'},
            {value: 'https://', key: '1'},
        ],
        activeMask: '0',
    };

    toggle = (tab) => {
        if (this.state.activeTab !== tab) this.setState({activeTab: tab});
    };

    toggleMaskHost = (activeMask) => {
        if (this.state.activeMask !== activeMask) this.setState({activeMask});
    };

    saveMathInfo() {
        setTimeout(() => {
            try {
                const listNumbers = this.getListNumbers().sort((a, b) => a - b);
                const median = getMedianValue(listNumbers);
                const {mode, multiMode} = getModeValue(listNumbers);
                const avg = getAvg(listNumbers);
                const standardDeviation = getStandardDeviation({listNumbers, avg});
                const mathInfo = {avg, standardDeviation, median, mode, multiMode};
                const listOne = getListByTaskOne(mathInfo);

                this.setState({fetching: false, mathInfo, listOne});
            } catch (e) {
                const errors = update(this.state.errors, {memory: {value: {$set: true}}});
                this.setState({fetching: false, errors, listOne: [{...errors.memory}]});
            }
        }, 150);
    }

    onChangeHost = (host) => this.setState({host});

    onPing = () => {
        const {host, maskList, activeMask} = this.state;
        const pingInstance = new Ping();
        let listTwo = [...this.state.listTwo];
        const maskHost = maskList[activeMask] ? maskList[activeMask].value : 'http://';

        pingInstance.ping(`${maskHost}${host}`, (err, ping) => {
            if (err) {
                listTwo = [{text: 'Неверный адрес', color: 'danger'}];
            } else {
                listTwo = [{text: `Время ответа в мс - ${ping}`, color: 'primary'}];
            }

            this.setState({listTwo, fetching: false});
        });
        this.setState({fetching: true});
    };

    onGenerateNumbers = () => {
        const {minValue, maxValue} = this.state;
        const emptyMin = minValue === '';
        const emptyMax = maxValue === '';
        const minMoreMax = minValue && maxValue && +minValue >= +maxValue;
        const errorMemory = false;
        const warnings = update(this.state.warnings, {
            emptyMin: {value: {$set: emptyMin}},
            emptyMax: {value: {$set: emptyMax}},
            minMoreMax: {value: {$set: minMoreMax}},
        });
        const errors = update(this.state.errors, {
            memory: {value: {$set: errorMemory}},
        });
        let fetching = false;
        let listOne = [...this.state.listOne];

        if (this.checkMinMaxValues()) {
            fetching = true;
            this.saveMathInfo();
        } else {
            listOne = Object.values(warnings).filter(item => item.value === true);
        }

        this.setState({warnings, fetching, errors, listOne});
    };

    onChangeMinValue = (minValue) => this.setState({minValue});

    onChangeMaxValue = (maxValue) => this.setState({maxValue});

    getListNumbers() {
        const {minValue, maxValue} = this.state;
        const countRandomValues = getRandomInt(minValue, maxValue);

        return [...new Array(countRandomValues).keys()].map(() => getRandomInt(minValue, maxValue));
    }

    getWarnings() {
        return this.state.warnings;
    }

    getErrors() {
        return this.state.errors;
    }

    checkErrors() {
        return Object.values(this.state.errors).some(item => item.value === true);
    }

    checkWarnings() {
        return Object.values(this.state.warnings).some(item => item.value === true);
    }

    checkErrorsAndWarnings() {
        return this.checkErrors() || this.checkWarnings();
    }

    checkMinMaxValues() {
        const {minValue, maxValue} = this.state;

        return minValue && maxValue && +maxValue > +minValue;
    };

    render() {
        const {minValue, maxValue, fetching, activeTab, listOne, host, maskList, activeMask, listTwo} = this.state;

        return <div className={style['main']}>
            <ProfitCenterLoader fetching={fetching}/>
            <ProfitCenterTabs activeTab={activeTab} toggle={this.toggle}/>
            <TabContent activeTab={activeTab}>
                <ProfitCenterTabOne min={minValue}
                                    max={maxValue}
                                    fetching={fetching}
                                    list={listOne}
                                    onChangeMinValue={this.onChangeMinValue}
                                    onChangeMaxValue={this.onChangeMaxValue}
                                    onGenerateNumbers={this.onGenerateNumbers}
                />
                <ProfitCenterTabTwo host={host}
                                    onChangeHost={this.onChangeHost}
                                    list={listTwo}
                                    onPing={this.onPing}
                                    maskList={maskList}
                                    activeMask={activeMask}
                                    toggleMaskHost={this.toggleMaskHost}
                />
            </TabContent>
        </div>
    }
}

export default ProfitCenterFx;