// Возвращает случайное целое число между min (включительно) и max (не включая max)
export const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (+max - +min)) + +min;
};

export const getMedianValue = (listNumbers) => {
    const numberLength = listNumbers.length;
    let middle = numberLength / 2;
    let medianValue = 0;

    if (numberLength % 2 === 1) {
        medianValue = listNumbers[parseInt(middle.toString())];
    } else {
        medianValue = (listNumbers[middle - 1] + listNumbers[middle]) / 2;
    }

    return medianValue;
};

export const getModeValue = (listNumbers) => {
    const objectNumbers = {};
    let max = null;
    let multiMode = false;
    let mode = [];

    listNumbers.forEach(item => {
        if (!objectNumbers[item]) objectNumbers[item] = {count: 0};
        objectNumbers[item].count++;
    });

    Object.keys(objectNumbers).forEach((key) => {
        let item = objectNumbers[key];
        if (item.count > 1 && item.count > max) {
            max = item.count;
            mode.push(key);
        }
    });

    if (!mode.length) {
        mode = 'Моды в этом множестве чисел нет';
    } else {
        if (mode.length > 1) multiMode = true;
        mode = mode.join(' ,');
    }

    return {mode, multiMode};
};

export const getStandardDeviation = ({listNumbers, avg}) => {
    const numberLength = listNumbers.length;
    const standardDeviation = listNumbers.reduce((prev, currentValue) => {
        let deviation = Math.pow((parseFloat(currentValue) - avg), 2);
        return prev + deviation;
    }, 0);
    return Math.sqrt(standardDeviation / numberLength);
};

export const getAvg = (listNumbers) => {
    const numberLength = listNumbers.length;
    const sumNumbers = listNumbers.reduce((prev, currentValue) => {
        return prev + currentValue;
    }, 0);

    return sumNumbers / numberLength;
};

export const getListByTaskOne = (mathInfo) => {
    const {avg, standardDeviation, median, mode, multiMode} = mathInfo;

    return [
        {
            color: 'primary',
            text: `Среднее значение - ${avg}`,
        },
        {
            color: 'primary',
            text: `Стандартное отклонение - ${standardDeviation}`,
        },
        {
            color: 'primary',
            text: `Медиана - ${median}`,
        },
        {
            color: 'primary',
            text: `Мода - ${mode} ${multiMode ? ' - мультимодальна' : ''}`,
        }
    ];
};