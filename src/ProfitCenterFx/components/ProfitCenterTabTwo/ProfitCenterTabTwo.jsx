import React, {Fragment} from 'react'
import {TabPane, Button, Label, Input, FormGroup} from 'reactstrap'
import style from './ProfitCenterTabTwo.css'
import ProfitCenterListOne from "../ProfitCenterListOne/ProfitCenterListOne"
import Box from "../Box/Box"
import cn from 'classnames'

class ProfitCenterTabTwo extends React.Component {

    toggleMaskHost = (key) => {
        this.props.toggleMaskHost(key);
    };

    render() {
        const {onChangeHost, onPing, host, maskList, activeMask, list} = this.props;

        return (
            <TabPane tabId="1">
                <ProfitCenterTaskTwo/>
                {maskList.map(item => {
                    const radioKey = item.key;
                    const radioValue= item.value;
                    const name = `radio${radioKey}`;

                    return <Fragment key={name}>
                        <FormGroup className={cn(style['margin_content'], style['form_group'])}>
                            <Label check>
                                <Input type="radio"
                                       name={name}
                                       onChange={this.toggleMaskHost.bind(this, radioKey)}
                                       checked={activeMask === radioKey}
                                />
                                {radioValue}
                            </Label>
                        </FormGroup>
                    </Fragment>
                })}
                <Box text='Host' placeholder='host' onChange={onChangeHost} value={host} disabledFocusStyle/>
                <ProfitCenterListOne list={list}/>
                <Button color="primary" className={style['button']} onClick={onPing}>Получить время</Button>
            </TabPane>
        );
    }
}

const ProfitCenterTaskTwo = () => {
    return <div className={style['header_text']}>
        <div>Написать пингер серверов на JavaScript, который покажет примерное время пинга сервера.</div>
    </div>;
};

export default ProfitCenterTabTwo;