import React, {PureComponent} from 'react'
import Box from '../Box/Box'

class NumberBox extends PureComponent {

    static defaultProps = {
        min: '1',
        max: '',
        notNull: true,
        value: '',
        text: '',
    };

    render() {
        const {placeholder, value, text} = this.props;

        return (
            <Box onChange={this.onChangeHandler}
                 placeholder={placeholder}
                 value={value}
                 text={text}
            />
        )
    }

    onChangeHandler = (value, e) => {
        value = this.trim(value).replace(/,/g, ".");

        if (this.props.notNull && value === '') value = this.props.min;

        if (value) {
            if (!this.checkValue(value)) return false;

            if (value[value.length - 1] !== '.') {
                value = this.getValue(value);
            }
        }

        this.props.onChange(`${+value}`, e);
    };

    checkValue(value) {
        if (value === '00' || value === '-00') return false;
        let reg = '^[-]?\\d+$';
        const regex = new RegExp(reg);

        return regex.test(value);
    }

    getValue(value) {
        if (this.props.max && value > this.props.max) {
            const integerValue = Math.trunc(value);

            if (integerValue > this.props.max) {
                return this.props.max;
            }
        }
        if (value < this.props.min) return this.props.min;

        return value;
    }

    trim = (value) => {
        return value.replace(/ /g, "");
    };
}

export default NumberBox;