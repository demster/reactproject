import React, {PureComponent} from 'react'
import style from './ProfitCenterField.css'

const WithProfitCenterField = (Component) => {

    return class ProfitCenterField extends PureComponent {
        render() {
            const {text} = this.props;

            return (
                <div className={style['number_box']}>
                    <div className={style['number_text']}>{text}</div>
                    <Component {...this.props} />
                </div>
            );
        }
    }
};

export default WithProfitCenterField;