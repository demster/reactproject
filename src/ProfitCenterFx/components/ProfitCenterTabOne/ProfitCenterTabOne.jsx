import React from 'react'
import {TabPane, Button} from 'reactstrap'
import style from './ProfitCenterTabOne.css'
import ProfitCenterListOne from "../ProfitCenterListOne/ProfitCenterListOne"
import NumberBox from "../NumberBox/NumberBox";

class ProfitCenterTabOne extends React.Component {

    render() {
        const {min, max, onChangeMinValue, onChangeMaxValue, onGenerateNumbers, list} = this.props;

        return (
            <TabPane tabId="0">
                <ProfitCenterTaskOne/>
                <NumberBox text='Min' placeholder='min' onChange={onChangeMinValue} value={min}/>
                <NumberBox text='Max' placeholder='max' onChange={onChangeMaxValue} value={max}/>
                <ProfitCenterListOne list={list}/>
                <Button color="primary" onClick={onGenerateNumbers} className={style['button']}>Сгенерировать
                    значения</Button>
            </TabPane>
        );
    }
}

const ProfitCenterTaskOne = () => {
    return <div className={style['header_text']}>
        <div>Необходимо создать веб интерфейс, который содержит два поля для ввода чисел: минимальное и
            максимальное.
        </div>
        <div>После ввода числе отправляется запрос с данными значениями
            из которых генерируется случайное количество чисел в диапазоне между минимальным и
            максимальным.
        </div>
        <div>Из сгенерированных чисел нужно посчитать: среднее значение, стандартное отклонение, моду и
            медиану
        </div>
    </div>;
};

export default ProfitCenterTabOne;