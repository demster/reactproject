import React, {PureComponent} from 'react'
import {Nav, NavItem, NavLink} from 'reactstrap'
import style from './ProfitCenterTabs.css'
import cn from "classnames"

class ProfitCenterTabs extends PureComponent {
    toggle = (tab) => {
        this.props.toggle(tab);
    };

    render() {
        const {activeTab} = this.props;

        return <Nav tabs>
            <NavItem className={style['nav_item']}>
                <NavLink
                    className={cn({active: activeTab === '0'})}
                    onClick={this.toggle.bind(this, '0')}
                >
                    Задание 1
                </NavLink>
            </NavItem>
            <NavItem className={style['nav_item']}>
                <NavLink
                    className={cn({active: activeTab === '1'})}
                    onClick={this.toggle.bind(this, '1')}
                >
                    Задание 2
                </NavLink>
            </NavItem>
        </Nav>
    }
}

export default ProfitCenterTabs;