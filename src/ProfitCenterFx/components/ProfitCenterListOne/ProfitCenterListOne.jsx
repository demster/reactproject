import React, {Fragment} from 'react'
import {Alert} from 'reactstrap'
import style from './ProfitCenterListOne.css'

class ProfitCenterListOne extends React.Component {
    render() {
        const {list} = this.props;

        return (
            <Fragment>
                {
                    list.map((item, index) => {
                        const {color, text} = item;

                        return <Fragment key={index}>
                            <Alert color={color} className={style['margin_content']}>
                                {text}
                            </Alert>
                        </Fragment>
                    })
                }
            </Fragment>
        );
    }
}

export default ProfitCenterListOne;