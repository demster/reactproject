import React, {PureComponent} from 'react'
import style from './Box.css'
import cn from 'classnames'
import WithProfitCenterField from "../ProfitCenterField/ProfitCenterField";

@WithProfitCenterField
class Box extends PureComponent {

    static defaultProps = {
        value: '',
        placeholder: '',
        disabledFocusStyle: false,
    };

    onChangeHandler = (e) => {
        this.props.onChange(e.target.value, e);
    };

    render() {
        const {placeholder, value, disabledFocusStyle} = this.props;
        const styleObject = cn(style['main'], {
            [style['disabled_focus']] : disabledFocusStyle,
        });

        return (
            <input onChange={this.onChangeHandler} placeholder={placeholder} value={value} className={styleObject}/>
        )
    }
}

export default Box;