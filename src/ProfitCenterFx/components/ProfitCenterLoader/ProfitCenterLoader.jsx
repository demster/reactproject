import React, {PureComponent} from 'react'
import style from './ProfitCenterLoader.css'
import Loader from 'react-loader-spinner'

class ProfitCenterLoader extends PureComponent {

    render() {
        const {fetching} = this.props;

        return (
            fetching ? <div className={style['spinner_wrap']}>
                <div className={style['spinner']}>
                    <Loader type="CradleLoader"
                            color="#00BFFF"
                            height="30"
                            width="100"
                    />
                </div>
            </div> : null
        );
    }
}

export default ProfitCenterLoader;


