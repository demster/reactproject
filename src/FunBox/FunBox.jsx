import React, {Fragment} from 'react';
import style from './FunBox.css';
import ProductContainer from "./components/ProductContainer/ProductContainer";
import FunBoxHelper from './helpers/helper';

export default class FunBox extends React.Component {

    state = {
        products: [
            {
                product: 'foie_gras',
                descriptionProduct: 'с фуа-гра',
                descriptionCountPortions: '10 порций',
                descriptionPresent: 'мышь в подарок',
                weight: '0,5',
                unit: 'кг',
                info: 'Печень утки разварная с артишоками.'
            },
            {
                product: 'fish',
                descriptionProduct: 'с рыбой',
                descriptionCountPortions: '40 порций',
                descriptionPresent: '2 мыши в подарок',
                weight: '2',
                unit: 'кг',
                info: 'Головы щучьи с чесноком да свежайшая сёмгушка.'
            },
            {
                product: 'chicken',
                descriptionProduct: 'с курицей',
                descriptionCountPortions: '100 порций',
                descriptionPresent: '5 мышей в подарок заказчик доволен',
                weight: '5',
                unit: 'кг',
                info: 'Филе из цыплят с трюфелями в бульоне.'
            }
        ],
        selected: [1],// index products  ... products[1]
        disabled: [2],// index products  ... products[2]
        hoverItems: [],
    };

    getStatusProductByIndex(index) {
        const {disabled, selected} = this.state;
        if (disabled.includes(index)) return FunBoxHelper.DISABLED_STATUS;

        let status = FunBoxHelper.DEFAULT_STATUS;
        if (selected.includes(index)) status = FunBoxHelper.SELECTED_STATUS;

        return status;
    }

    onChangeHoverProducts = (product) => {
        const {products, hoverItems: stateHoverItems} = this.state;
        const hoverItems = [...stateHoverItems];
        const findProductIndex = products.findIndex(item => item.product === product);
        const findSelectedIndex = hoverItems.findIndex(item => item === findProductIndex);

        if (findSelectedIndex === -1) {
            hoverItems.push(findProductIndex);
        }

        this.setState({hoverItems});
    };

    onChangeSelectProducts = (product) => {
        const {products, selected: stateSelected/*, hoverItems: stateHoverItems*/} = this.state;
        const selected = [...stateSelected];
        //const hoverItems = [...stateHoverItems];
        const findProductIndex = products.findIndex(item => item.product === product);
        const findSelectedIndex = selected.findIndex(item => item === findProductIndex);
        //const findHoverIndex = hoverItems.findIndex(item => item === findProductIndex);

        if (findSelectedIndex !== -1) {
            selected.splice(findSelectedIndex, 1);
        } else {
            selected.push(findProductIndex);
        }

        //if (findHoverIndex !== -1) hoverItems.splice(findSelectedIndex, 1);

        this.setState({selected});
        // this.setState({selected, hoverItems});
    };

    render() {
        const {products, hoverItems} = this.state;

        return (
            <div className={style['main']}>
                <div className={style['header']}>Ты сегодня покормил кота?</div>
                <div className={style['content']}>
                    {products.map((item, index) => {
                        const status = this.getStatusProductByIndex(index);
                        const hover = hoverItems.includes(index);
                        const selected = status === FunBoxHelper.SELECTED_STATUS;
                        const disabled = status === FunBoxHelper.DISABLED_STATUS;

                        return <Fragment key={`${item.product}-${index}`}>
                            <ProductContainer {...item} status={status} hover={hover} selected={selected}
                                              disabled={disabled} onChangeSelectProducts={this.onChangeSelectProducts}
                                              onChangeHoverProducts={this.onChangeHoverProducts}/>
                        </Fragment>
                    })}
                </div>
            </div>
        );
    }
}