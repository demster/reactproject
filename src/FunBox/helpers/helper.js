export default class FunBoxHelper {
    static PRODUCT = 'Нямушка';
    static DESCRIPTION = 'Сказочное заморское явство';
    static DESCRIPTION_BAD = 'Котэ не одобряет?';
    static DEFAULT_STATUS = 0; // по умолчанию
    static SELECTED_STATUS = 1; // выбран
    static DISABLED_STATUS = 2; // нет в наличии

    static getBackgroundAngle(backgroundColor) {
        const linearFrom = 'linear-gradient(135deg, transparent 34px, #f2f2f2 0)';
        const linearTo = `linear-gradient(135deg, transparent 30px, ${backgroundColor} 0)`;

        return `${linearFrom}, ${linearTo}`;
    }

    static getBackgroundWeightByStatus({status, hover}) {
        const {DEFAULT_STATUS, SELECTED_STATUS, DISABLED_STATUS} = FunBoxHelper;
        const relations = {
            [DEFAULT_STATUS]: hover ? '#2ea8e6' : '#1698d9',
            [SELECTED_STATUS]: hover ? '#e62e7a' : '#d91667',
            [DISABLED_STATUS]: '#b3b3b3',
        };

        return relations[status];
    }

    static getInfoItemByStatus({info, status, descriptionProduct}){
        const {DEFAULT_STATUS, SELECTED_STATUS, DISABLED_STATUS} = FunBoxHelper;
        const relations = {
            [DEFAULT_STATUS]: 'Чего сидишь? Порадуй котэ,',
            [SELECTED_STATUS]: info,
            [DISABLED_STATUS]: `Печалька, ${descriptionProduct} закончился.`,
        };

        return relations[status];
    }
}

