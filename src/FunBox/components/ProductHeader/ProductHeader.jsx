import React from 'react';
import style from './ProductHeader.css';
import FunBoxHelper from '../../helpers/helper';

export default class ProductHeader extends React.Component {

    static defaultProps = {
        status: FunBoxHelper.DEFAULT_STATUS,
    };

    render() {
        const {status, hover, selected} = this.props;
        const {DESCRIPTION, DESCRIPTION_BAD} = FunBoxHelper;
        const backgroundColor = FunBoxHelper.getBackgroundWeightByStatus({status, hover});
        const backgroundHeader = FunBoxHelper.getBackgroundAngle(backgroundColor);
        const text = selected && hover ? DESCRIPTION_BAD : DESCRIPTION;
        const descriptionColor = selected && hover ? backgroundColor : null;

        return (
            <div className={style['header']}>
                <div className={style['header_left']} style={{background: backgroundHeader}}/>
                <div className={style['header_right']} style={{borderColor: backgroundColor}}>
                    <div className={style['description']} style={{color: descriptionColor}}>{text}</div>
                </div>
            </div>
        );
    }
}