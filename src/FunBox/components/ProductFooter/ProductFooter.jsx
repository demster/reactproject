import React from 'react';
import style from './ProductFooter.css';
import FunBoxHelper from "../../helpers/helper";
import cn from 'classnames';

export default class ProductFooter extends React.Component {

    renderSelectButton = () => {
        const {status, onChangeSelectProducts} = this.props;
        const showSelectButton = status === FunBoxHelper.DEFAULT_STATUS;

        //Без мемоизации.
        return showSelectButton ?
            <span className={style['button']} onClick={onChangeSelectProducts}>купи.</span> : null;
    };

    render() {
        const {info, status, descriptionProduct, disabled} = this.props;
        const text = FunBoxHelper.getInfoItemByStatus({info, status, descriptionProduct});
        const styleButton = cn(style['main'], {
            [style['disabled']] : disabled
        });

        return (
            <div className={styleButton}>{text}{this.renderSelectButton()}</div>
        );
    }
}