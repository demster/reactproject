import React from 'react';
import style from './ProductContainer.css';
import ProductHeader from "../ProductHeader/ProductHeader";
import ProductContent from "../ProductContent/ProductContent";
import ProductFooter from "../ProductFooter/ProductFooter";
import cn from 'classnames';

export default class ProductContainer extends React.Component {

    constructor(props) {
        super(props);
        const {product, disabled} = props;
        this.onSelect = !disabled ? this.onSelect.bind(this, product) : null;
        this.onMouseLeave = !disabled ? this.onMouseLeave.bind(this, product) : null;
    }

    onMouseLeave = (product) => {
        this.props.onChangeHoverProducts(product);
    };

    onSelect = (product) => {
        this.props.onChangeSelectProducts(product);
    };

    render() {
        const {status, info, descriptionProduct, hover, selected, disabled} = this.props;
        const styleProductContainer = cn(style['product_container'], {
            [style['disabled']] : disabled,
        });

        return (
            <div className={style['main']}>
                <div className={styleProductContainer} onClick={this.onSelect} onMouseLeave={this.onMouseLeave}>
                    <ProductHeader status={status} hover={hover} selected={selected}/>
                    <ProductContent {...this.props}/>
                </div>
                <ProductFooter status={status} info={info} descriptionProduct={descriptionProduct}
                               onChangeSelectProducts={this.onSelect}/>
            </div>
        );
    }
}