import React from 'react';
import style from './ProductContent.css';
import Circle from "../../../Core/Circle/Circle";
import FunBoxHelper from '../../helpers/helper';

export default class ProductContent extends React.Component {

    static defaultProps = {
        descriptionProduct: 'с фуа-гра',
        descriptionCountPortions: '10 порций',
        descriptionPresent: 'мышь в подарок',
        weight: '0,5',
        unit: 'кг',
        status: FunBoxHelper.DEFAULT_STATUS,
    };

    render() {
        const {descriptionProduct, descriptionCountPortions, descriptionPresent, weight, unit, status, hover} = this.props;
        const {PRODUCT} = FunBoxHelper;
        const backgroundColor = FunBoxHelper.getBackgroundWeightByStatus({status, hover});

        return (
            <div className={style['border_container']} style={{borderColor: backgroundColor}}>
                <div className={style['info_container']}>
                    <div className={style['product']}>{PRODUCT}</div>
                    <div className={style['description_product']}>{descriptionProduct}</div>
                    <div className={style['description_present_container']}>
                        <div>{descriptionCountPortions}</div>
                        <div>{descriptionPresent}</div>
                    </div>
                    <div className={style['circle_wrap']}>
                        <Circle width={81} height={81} text={weight} unit={unit} background={backgroundColor}/>
                    </div>
                </div>
                <div className={style['cat_photo']}/>
            </div>
        );
    }
}