import {connect} from 'react-redux';
import React from 'react';
import {bindActionCreators} from 'redux';
import BeejeeApp from '../components/BeejeeApp/BeejeeApp';
import * as beejeeActions from '../actions/beejeeActions';

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(beejeeActions, dispatch),
    };
}

function mapStateToProps(store) {
    return {
        store: store.beejeeState,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BeejeeApp);
