import React, {Fragment} from 'react';
import style from './BeejeeFooter.css';
import cn from 'classnames';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default class BeejeeFooter extends React.Component {

    renderPages() {
        const pageCount = 300 / 3;
        const pages = [...new Array(pageCount)];

        return (
            pages.map((item, index) => {
                return <Fragment key={`task-${item}`}>
                    <div className={style['pagination_item']}>{index+1}</div>
                </Fragment>
            })
        );
    };

    render() {
        const {activePage, tasks, onChangePage} = this.props;
        const taskCount = tasks.length;
        const activeItemClass = cn(style['pagination_item'], {
            [style['active_item']] : taskCount === 0,
        });

        return (
            <div className={style['main']}>
                <Pagination aria-label="Page navigation example">
                    <PaginationItem>
                        <PaginationLink previous href="#" >Previos</PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink href="#">
                            1
                        </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink href="#">
                            2
                        </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink href="#">
                            3
                        </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink href="#">
                            4
                        </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink href="#">
                            5
                        </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink next href="#" >Next</PaginationLink>
                    </PaginationItem>
                </Pagination>
            </div>
        );
    }
};