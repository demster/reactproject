import React from 'react';
import style from './BeejeeApp.css';
import BeejeeHeader from "../BeejeeHeader/BeejeeHeader";
import BeejeeContent from "../BeejeeContent/BeejeeContent";
import BeejeeFooter from "../BeejeeFooter/BeejeeFooter";
import {Form} from "react-bootstrap";

export default class BeejeeApp extends React.Component {

    render() {
        const {tasks} = this.props.store;
        return (
            <div className={style['main']}>
                <Form className={style['form']}>
                    <BeejeeHeader/>
                    <BeejeeContent/>
                    <BeejeeFooter tasks={tasks}/>
                </Form>
            </div>
        )
    }
};