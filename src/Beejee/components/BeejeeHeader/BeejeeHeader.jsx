import React from 'react';
import style from './BeejeeHeader.css';
import Button from "../../../Core/Button/Button";

export default class BeejeeHeader extends React.Component {
    render() {
        return (
            <div className={style['main']}>
                <div className={style['header_text']}>Приложение-задачник</div>
                <Button text={'Войти'} type={'login'}/>
            </div>
        );
    }
};