const prefix = 'BEEJEE_';

const actionTypes = {
	GET_DATA: prefix + 'GET_DATA_CRM',
};

export default actionTypes;