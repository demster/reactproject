import React from 'react';
import style from './Circle.css';

export default class Circle extends React.Component {

    render(){
        const {text, unit, width, height, background} = this.props;

        return (
            <div className={style['main']} style={{width, height, background}}>
                <div className={style['text']}>{text}</div>
                {unit && <div className={style['unit']}>{unit}</div>}
            </div>
        );
    }
}