import React from 'react';
import style from './Button.css';
import cn from 'classnames';

export default class Button extends React.Component {

    render(){
        const {text, type, readOnly} = this.props;
        const styleButton = cn(style['button'], {
            [style['login']]: type === 'login',
            [style['read_only']]: readOnly,
        });

        return (
            <div className={styleButton}>
                <div className={style['text']}>{text}</div>
            </div>
        );
    }
};